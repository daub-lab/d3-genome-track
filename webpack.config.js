const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
module.exports = {
    entry: ["./index.js"],
    output: {
        path: __dirname + '/dist',
        filename: "d3-genome-track.js",
        library: "d3"
    },
    module: {
        rules: [
        {
        test: /\.(js|jsx)$/,
    exclude: /node_modules/,
    use: ['babel-loader']
        }
        ]
    },
    optimization: {
    minimizer: [
      new UglifyJSPlugin({
        sourceMap: true,
        uglifyOptions: {
          compress: {
            inline: false
          }
        }
      })
    ],
    runtimeChunk: false,
    splitChunks: {
      cacheGroups: {
        default: false,
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor_app',
          chunks: 'all',
          minChunks: 2
        }
      }
    }
  }
};
