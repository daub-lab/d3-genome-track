# d3-genome-track

A proof-of-concept for a minimalistic genome browser track.

[Live demo](https://daub-lab.gitlab.io/d3-genome-track/)

## Background
The module was developed with the following design principles:
### Visual noise reduction
Classic genome browser show a high visual redundancy, where one track only shows one type of information. A typical minimal setup for a genome browser consist of a track for the genome coordinates, a track for the reference genome, a track with a sequencing experiment signal. All of them are always shown with the same importance.

We propose a different approach, where for example the above three tracks a merged into one, with additional information, e.g. the coordinates and annotation only visible if the user hovers over them. These allows for a clearer inspection and comparisson of the measured signal.

### Animation
Similar to the problem above, viewing multiple tracks for example from different biological samples can be cumbersome and hinder visual comparison.

Our approach to solve this is to flip through the different track data inside one track, creating an animated view in that way. 

Taking this idea one step further, one can even cycle through different locations in one or more samples inside the same track view. This animation allows to inspect different signal patterns, e.g. enhancer expression in different genomic locations and is not possible in a single instance of the common genome browsers. 
