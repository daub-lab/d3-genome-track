import { select } from 'd3-selection';
import { transition } from 'd3-transition';
import { easeQuad } from 'd3-ease';
import { annotation, annotationLabel } from 'd3-svg-annotation';
import { max } from 'd3-array';
import { scaleLinear, scaleOrdinal } from 'd3-scale';
import { axisBottom, axisLeft } from 'd3-axis';
import 'pileup';
import ContigInterval from '../node_modules/pileup/dist/main/ContigInterval.js';
/* eslint-enable */
var range = { contig: 'chr17', start: 500004, stop: 7512644 };
range = new ContigInterval(range.contig, range.start, range.stop);
var bamSource = pileup.formats.bam({
	url: './test-data/synth3.normal.17.7500000-7515000.bam',
	indexUrl: './test-data/synth3.normal.17.7500000-7515000.bam.bai'
});
var bigbed = pileup.formats.bigBed({
	url: 'http://www.biodalliance.org/datasets/ensGene.bb'
});
bamSource.on('newdata', range => {
	console.log(bamSource.getAlignmentsInRange(range));
});

// console.log('bigbed',bigbed.getFeaturesInRange(range), 'bamSource',bamSource.getAlignmentsInRange(range))

function histoBarTrack() {
	// All options that should be accessible to caller
	let margin = {
			top: 0,
			right: 50,
			bottom: 0,
			left: 50
		},
		width = 1500 - margin.left - margin.right,
		height = 250 - margin.top - margin.bottom,
		fillColors = ['#f03e3e', '#1098AD'],
		data = [];
	var updateHeight, updateWidth, updateFillColors, updateData;

	function chart(selection) {
		selection.each(function() {
			let minmax = d3.extent(data.feature, d => {
				return parseFloat(d._strand + d._sig);
			});
			var absmax = Math.abs(Math.min.apply(null, minmax));
			let xScale = scaleLinear()
				.domain([Math.floor(data._start), Math.floor(data._end)])
				.range([0, width]);
			let yScale = scaleLinear()
				.domain([-464, 464])
				// .domain([-24, 24])
				.range([height, 0]);
			let xAxis = axisBottom(xScale).tickSizeOuter(0);
			let yAxis = axisLeft(yScale)
				.ticks(2)
				.tickSizeOuter(0);
			let dom = select(this);
			let svg = dom
				.append('svg')
				.attr('class', 'histoBarTrack')
				.attr('height', height + margin.top + margin.bottom)
				.attr('width', width + margin.left + margin.right)
				.append('g')
				.attr(
					'transform',
					'translate(' + margin.left + ',' + margin.top + ')'
				);
			var color = scaleOrdinal()
				.domain(['+', '-'])
				.range(fillColors);
			svg
				.append('text')
				.attr('class', 'title')
				.attr('x', width / 2)
				.attr('y', 0 - margin.top / 2)
				.attr('text-anchor', 'middle')
				.style('font-size', '16px')
				.text(data.sources.experiment._name);
			let bar = svg
				.selectAll('rect.histoBar')
				.data(data.feature, d => d._name);

			bar
				.enter()
				.append('rect')
				.attr('class', 'histoBar')
				.attr('width', d => {
					return Math.max(xScale(d._end) - xScale(d._start), 2);
				})
				.attr('x', d => {
					return xScale(d._start);
				})
				.attr('y', d => {
					return d._strand === '-' ? yScale(0) : yScale(d._sig);
				})
				.attr('fill', d => {
					return color(d._strand);
				})
				.attr('height', d => {
					// return y(d._sig)
					// return (Math.abs(yScale(d._sig) - yScale(0)));
					return Math.max(
						Math.abs(yScale(d._sig) - yScale(0)),
						parseFloat(2)
					);
					// return (Math.abs(yScale(absmax*Math.random()) - yScale(0)));
				});
			svg
				.append('g')
				.attr('class', 'axis axis--x')
				.attr('transform', `translate(0,${yScale(0)})`)
				.call(xAxis);
			svg
				.append('g')
				.attr('class', 'axis axis--y')
				// .attr('transform', `translate(0,${y(0)})`)
				.call(yAxis);
			svg
				.selectAll('.axis--y .tick')
				.filter(function(d) {
					return d === 0;
				})
				.remove();
			// update functions
			updateWidth = function() {
				xScale = scaleLinear().range([0, width]);
				bar
					.transition()
					.duration(1000)
					.attr('width', d => {
						return xScale(d._end) - xScale(d._start);
					})
					.attr('x', d => {
						return xScale(d._start);
					});
				svg
					.transition()
					.duration(1000)
					.attr('width', width);
			};

			updateHeight = function() {
				yScale = scaleLinear().range([height, 0]);
				// bars.transition().duration(1000).attr('y', function(d, i) {
				// 	return i * barSpacing;
				// }).attr('height', barHeight);
				svg
					.transition()
					.duration(1000)
					.attr('height', height)
					.attr('height', d => {
						// return y(d._sig)
						// return (Math.abs(yScale(d._sig) - yScale(0)));
						return Math.max(
							Math.abs(yScale(d._sig) - yScale(0)),
							parseFloat(d._strand + 2)
						);
					});
			};

			updateFillColors = function() {
				svg
					.transition()
					.duration(1000)
					.style('fill', fillColors);
			};

			updateData = function(data) {
				let t = d3
					.transition()
					.duration(300)
					.ease(easeQuad);
				let minmax = d3.extent(data.feature, d => {
					return parseFloat(d._strand + d._sig);
				});
				let absmax_tmp = Math.abs(Math.min.apply(null, minmax));

				let xScale = scaleLinear()
					.domain([Math.floor(data._start), Math.floor(data._end)])
					.range([0, width]);
				if (absmax < absmax_tmp) {
					absmax = absmax_tmp;
					console.log(absmax);
				}
				let yScale = scaleLinear()
					.domain([-absmax, absmax])
					.range([height, 0]);
				let xAxis = axisBottom(xScale).tickSizeOuter(0);
				let yAxis = axisLeft(yScale)
					.ticks(2)
					.tickSizeOuter(0);
				svg
					.selectAll('text.title')
					.text(data.sources.experiment._name);
				let update = svg
					.selectAll('rect.histoBar')
					.data(data.feature, d => d._name);

				update
					.exit()
					.transition(t)
					.style('opacity', 0)
					.attr('height', 0)
					.attr('x', 0)
					.attr('width', 0)
					.remove();

				let enter = update
					.enter()
					.append('rect')
					.attr('class', 'histoBar')
					.attr('x', d => {
						return xScale(d._start);
					})
					.attr('y', () => yScale(0));
				update
					.merge(enter)
					.transition(t)
					.attr('width', d => {
						return Math.max(xScale(d._end) - xScale(d._start), 2);
					})
					.attr('x', d => {
						return xScale(d._start);
					})
					.attr('y', d => {
						return d._strand === '-' ? yScale(0) : yScale(d._sig);
					})
					.attr('height', d => {
						// return y(d._sig)
						// return (Math.abs(yScale(d._sig) - yScale(0)));
						return Math.max(
							Math.abs(yScale(d._sig) - yScale(0)),
							1
						);
					})
					.attr('fill', d => {
						return color(d._strand);
					})
			};
		});
	}

	chart.width = function(value) {
		if (!arguments.length) return width;
		width = value - margin.left - margin.right;
		if (typeof updateWidth === 'function') updateWidth();
		return chart;
	};

	chart.height = function(value) {
		if (!arguments.length) return height;
		height = value;
		if (typeof updateHeight === 'function') updateHeight();
		return chart;
	};

	chart.fillColors = function(value) {
		if (!arguments.length) return fillColors;
		fillColors = value;
		if (typeof updateFillColors === 'function') updateFillColors();
		return chart;
	};

	chart.data = function(value) {
		if (!arguments.length) return data;
		data = value;
		if (typeof updateData === 'function') updateData(data);
		return chart;
	};

	return chart;
}

function blockTrack() {
	// All options that should be accessible to caller
	let margin = {
			top: 0,
			right: 50,
			bottom: 0,
			left: 50
		},
		width = 1500 - margin.left - margin.right,
		height = 250 - margin.top - margin.bottom,
		fillColors = ['#f03e3e', '#1098AD'],
		data = [];
	var updateHeight, updateWidth, updateFillColors, updateData;

	function chart(selection) {
		selection.each(function() {
			let xScale = scaleLinear()
				.domain([Math.floor(data._start), Math.floor(data._end)])
				.range([0, width]);
			let yScale = scaleOrdinal()
				.domain(['-', '+'])
				.range([height / 2, 0]);
			let svg = select('#' + this.id + ' svg')
				.append('g')
				.attr(
					'transform',
					'translate(' + margin.left + ',' + margin.top + ')'
				);
			let color = scaleOrdinal()
				.domain(['+', '-'])
				.range(fillColors);
			let bar = svg
				.selectAll('rect.block')
				.data(data.feature, d => d._name);

			bar
				.enter()
				.append('rect')
				.attr('class', 'block')
				.attr('width', d => {
					var width = Math.max(xScale(d._end) - xScale(d._start), 1);

					if (xScale(d._start) < 0) {
						width = Math.max(
							xScale(d._end) - xScale(data._start),
							1
						);
					}
					if (xScale(d._end)>xScale.range()[1]){
						width = Math.max(
							xScale(data._end) - xScale(d._start),
							1
						);
					}

					return width;
				})
				.attr('x', d => {
					return xScale(d._start) < 0
						? xScale(data._start)
						: xScale(d._start);
				})
				.attr('y', d => {
					return yScale(d._strand);
				})
				.attr('fill', d => {
					return color(d._strand);
				})
				.style('opacity', 0.25)
				.attr('height', d => {
					// return y(d._sig)
					// return (Math.abs(yScale(d._sig) - yScale(0)));
					return height / 2;
					// return (Math.abs(yScale(absmax*Math.random()) - yScale(0)));
				})
				.on('mouseover', function(d) {
					showLabel(d);
				})
				.on('mouseout', function() {
					// should think of a better way of doing this
					// ideally should be showing/hiding not appending/removing
					d3.select('.annotation-group').remove();
				});
			function showLabel(d) {
				var annotations = [
					{
						note: {
							label: d._name
							// title: d.party
						},
						data: {
							start: d._start,
							end: d._end,
							strand: d._strand
						},
						subject: {
							x1: xScale(d._start),
							x2: xScale(d._end)
						},
						dy: d._strand === '+' ? 10 : -10,
						dx: 0
					}
				];

				var makeAnnotations = annotation()
					.type(annotationLabel)
					.accessors({
						x: function(d) {
							if (xScale(d.start) < 0) {
								return xScale(d.end) / 2;
							} else if (xScale(d.end)>xScale.range()[1]){
								return (xScale(d.start) + xScale(data._end)) / 2;
							} else {
								return (xScale(d.start) + xScale(d.end)) / 2;
							}
						},
						y: function() {
							return height / 2;
						}
					})
					.annotations(annotations);

				var annotationsGroup = svg
					.append('g')
					.attr('class', 'annotation-group')
					.call(makeAnnotations);
			}
			// update functions
			updateWidth = function() {
				xScale = scaleLinear().range([0, width]);
				bar
					.transition()
					.duration(1000)
					.attr('width', d => {
						return xScale(d._end) - xScale(d._start);
					})
					.attr('x', d => {
						return xScale(d._start);
					});
				svg
					.transition()
					.duration(1000)
					.attr('width', width);
			};

			updateHeight = function() {
				yScale = scaleLinear().range([height, 0]);
				// bars.transition().duration(1000).attr('y', function(d, i) {
				// 	return i * barSpacing;
				// }).attr('height', barHeight);
				svg
					.transition()
					.duration(1000)
					.attr('height', height)
					.attr('height', d => {
						// return y(d._sig)
						// return (Math.abs(yScale(d._sig) - yScale(0)));
						return Math.max(
							Math.abs(yScale(d._sig) - yScale(0)),
							parseFloat(d._strand + 2)
						);
					});
			};

			updateFillColors = function() {
				svg
					.transition()
					.duration(1000)
					.style('fill', fillColors);
			};

			updateData = function(data) {
				let t = d3
					.transition()
					.duration(300)
					.ease(easeQuad);
				let minmax = d3.extent(data.feature, d => {
					return parseFloat(d._strand + d._sig);
				});
				let absmax = Math.max.apply(null, minmax.map(Math.abs));

				let xScale = scaleLinear()
					.domain([Math.floor(data._start), Math.floor(data._end)])
					.range([0, width]);
				// let yScale = scaleLinear().domain([
				// 	-absmax,
				// 	absmax
				// ]).range([height, 0]);
				svg
					.selectAll('text.title')
					.text(data.sources.featuresource._name);
				let update = svg
					.selectAll('rect.block')
					.data(data.feature, d => d._name);

				update
					.exit()
					.transition(t)
					.style('opacity', 0)
					.attr('height', 0)
					.attr('x', 0)
					.attr('width', 0)
					.remove();

				let enter = update
					.enter()
					.append('rect')
					.attr('class', 'block')
					.attr('x', d => {
						return xScale(d._start);
					})
					.attr('y', () => yScale(0));
				update
					.merge(enter)
					.transition(t)
					.attr('width', d => {
						return xScale(d._end) - xScale(d._start);
					})
					.attr('x', d => {
						return xScale(d._start);
					})
					.attr('y', d => {
						return d._strand === '-' ? yScale(0) : yScale(d._sig);
					})
					.attr('height', d => {
						// return y(d._sig)
						// return (Math.abs(yScale(d._sig) - yScale(0)));
						return Math.max(
							Math.abs(yScale(d._sig) - yScale(0)),
							1
						);
					})
					.style('fill', fillColors);
			};
		});
	}

	chart.width = function(value) {
		if (!arguments.length) return width;
		width = value - margin.left - margin.right;
		if (typeof updateWidth === 'function') updateWidth();
		return chart;
	};

	chart.height = function(value) {
		if (!arguments.length) return height;
		height = value;
		if (typeof updateHeight === 'function') updateHeight();
		return chart;
	};

	chart.fillColors = function(value) {
		if (!arguments.length) return fillColors;
		fillColors = value;
		if (typeof updateFillColors === 'function') updateFillColors();
		return chart;
	};

	chart.data = function(value) {
		if (!arguments.length) return data;
		data = value;
		if (typeof updateData === 'function') updateData(data);
		return chart;
	};

	return chart;
}

function histoDotTrack() {
	// All options that should be accessible to caller
	let margin = {
			top: 30,
			right: 50,
			bottom: 30,
			left: 50
		},
		width = 1500 - margin.left - margin.right,
		height = 150 - margin.top - margin.bottom,
		fillColors = ['#f03e3e', '#1098AD'],
		data = [];
	var updateHeight, updateWidth, updateFillColors, updateData;

	function chart(selection) {
		selection.each(function() {
			let minmax = d3.extent(data.feature, d => {
				return parseFloat(d._strand + d._sig);
			});
			let absmax = Math.max.apply(null, minmax.map(Math.abs));

			let xScale = scaleLinear()
				.domain([Math.floor(data._start), Math.floor(data._end)])
				.range([0, width]);
			let yScale = scaleLinear()
				.domain([-absmax, absmax])
				.range([height, 0]);
			let xAxis = axisBottom(xScale).tickSizeOuter(0);
			let yAxis = axisLeft(yScale)
				.ticks(2)
				.tickSizeOuter(0);
			let dom = select(this);
			let svg = dom
				.append('svg')
				.attr('class', 'histoBarTrack')
				.attr('height', height + margin.top + margin.bottom)
				.attr('width', width + margin.left + margin.right)
				.append('g')
				.attr(
					'transform',
					'translate(' + margin.left + ',' + margin.top + ')'
				);
			let color = scaleOrdinal()
				.domain(['+', '-'])
				.range(fillColors);
			svg
				.append('text')
				.attr('class', 'title')
				.attr('x', width / 2)
				.attr('y', 0 - margin.top / 2)
				.attr('text-anchor', 'middle')
				.style('font-size', '16px')
				.text(data.sources.featuresource._name);
			let dot = svg
				.selectAll('rect.histoBar')
				.data(data.feature, d => d._name);

			dot
				.enter()
				.append('circle')
				.attr('class', 'histoDot')
				.attr('r', d => {
					return xScale(d._end) - xScale(d._start);
				})
				.attr('cx', function(d) {
					return (xScale(d._end) + xScale(d._start)) / 2;
				})
				.attr('cy', function(d) {
					return yScale(parseFloat(d._strand + d._sig));
				})
				.attr('fill', d => {
					return color(d._strand);
				});
			svg
				.append('g')
				.attr('class', 'axis axis--x')
				.attr('transform', `translate(0,${yScale(0)})`)
				.call(xAxis);
			svg
				.append('g')
				.attr('class', 'axis axis--y')
				// .attr('transform', `translate(0,${y(0)})`)
				.call(yAxis);
			svg
				.selectAll('.axis--y .tick')
				.filter(function(d) {
					return d === 0;
				})
				.remove();
			// update functions
			updateWidth = function() {
				xScale = scaleLinear().range([0, width]);
				dot
					.transition()
					.duration(1000)
					.attr('r', d => {
						return xScale(d._end) - xScale(d._start);
					})
					.attr('cx', d => {
						return (xScale(d._end) + xScale(d._start)) / 2;
					});
				svg
					.transition()
					.duration(1000)
					.attr('width', width);
			};

			updateHeight = function() {
				yScale = scaleLinear().range([height, 0]);
				// bars.transition().duration(1000).attr('y', function(d, i) {
				// 	return i * barSpacing;
				// }).attr('height', barHeight);
				svg
					.transition()
					.duration(1000)
					.attr('height', height)
					.attr('height', d => {
						// return y(d._sig)
						return Math.abs(yScale(d._sig) - yScale(0));
					});
			};

			updateFillColors = function() {
				svg
					.transition()
					.duration(1000)
					.style('fill', fillColors);
			};

			updateData = function(data) {
				let t = d3
					.transition()
					.duration(300)
					.ease(easeQuad);
				let minmax = d3.extent(data.feature, d => {
					return parseFloat(d._strand + d._sig);
				});
				let absmax = Math.max.apply(null, minmax.map(Math.abs));

				let xScale = scaleLinear().range([0, width]);
				let yScale = scaleLinear().range([height, 0]);
				let xAxis = axisBottom(xScale).tickSizeOuter(0);
				let yAxis = axisLeft(yScale)
					.ticks(2)
					.tickSizeOuter(0);
				svg
					.selectAll('text.title')
					.text(data.sources.featuresource._name);
				var update = svg
					.selectAll('rect.histoBar')
					.data(data, d => d._name);

				update
					.exit()
					.transition(t)
					.style('opacity', 0)
					.attr('height', 0)
					.attr('x', 0)
					.attr('width', 0)
					.remove();

				update
					.transition()
					.duration(1000)
					.attr('width', d => {
						return xScale(d._end) - xScale(d._start);
					})
					.attr('x', d => {
						return xScale(d._start);
					})
					.attr('y', d => {
						return d._strand === '-' ? yScale(0) : yScale(d._sig);
					})
					.attr('height', d => {
						// return y(d._sig)
						return Math.abs(yScale(d._sig) - yScale(0));
					});

				var enter = update
					.enter()
					.append('circle')
					.attr('class', 'histoDot')
					.attr('r', d => {
						return xScale(d._end) - xScale(d._start);
					})
					.attr('cx', function(d) {
						return (xScale(d._end) + xScale(d._start)) / 2;
					})
					.attr('cy', function(d) {
						return yScale(parseFloat(d._strand + d._sig));
					})
					.attr('fill', d => {
						return color(d._strand);
					});
			};
		});
	}

	chart.width = function(value) {
		if (!arguments.length) return width;
		width = value - margin.left - margin.right;
		if (typeof updateWidth === 'function') updateWidth();
		return chart;
	};

	chart.height = function(value) {
		if (!arguments.length) return height;
		height = value;
		if (typeof updateHeight === 'function') updateHeight();
		return chart;
	};

	chart.fillColors = function(value) {
		if (!arguments.length) return fillColors;
		fillColors = value;
		if (typeof updateFillColors === 'function') updateFillColors();
		return chart;
	};

	chart.data = function(value) {
		if (!arguments.length) return data;
		data = value;
		if (typeof updateData === 'function') updateData(data);
		return chart;
	};

	return chart;
}

export { histoBarTrack, histoDotTrack, blockTrack };
