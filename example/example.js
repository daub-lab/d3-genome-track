document.addEventListener("DOMContentLoaded", function() {

	var data = [];
	var split = false;
	let play = true;
	var animation = null;
	var animationIndex = 1;
	var reference = true;
	var graph = "bar"
	$('#btn-play').hide()
	$('#btn-show-reference').hide()
	$('#singletrack').hide()

	$('#split-group').on('click', function(e) {
		if (e.target.id === "singletrack") {
			split = false;
			play = true;
			$('#btn-play').hide()
			$('#btn-pause').show()
			$('#singletrack').attr('disabled', true)
			$('#multitrack').attr('disabled', false)
			$('#animation-group').show()
			$('#singletrack').hide()
			$('#multitrack').show()
		} else if (e.target.id === "multitrack") {
			split = true;
			$('#singletrack').attr('disabled', false)
			$('#multitrack').attr('disabled', true)
			$('#animation-group').hide()

			$('#singletrack').show()
			$('#multitrack').hide()

		};
		draw()
	})
	$('#graph-group').on('click', function(e) {
		if (e.target.id === "barGraph") {
			graph = "bar"
			$('#barGraph').attr('disabled', true)
			$('#dotGraph').attr('disabled', false)
			// $('#animation-group').show()
		} else if (e.target.id === "dotGraph") {
			graph = "dot"
			$('#barGraph').attr('disabled', false)
			$('#dotGraph').attr('disabled', true)
			// $('#animation-group').hide()

		};
		draw()
	})
	$('.btn-animation').on('click', function(e) {
		let id = this.id;
		switch (id) {
			case 'btn-pause':
				play = false;
				$('#btn-pause').hide()
				$('#btn-play').show()
				clearInterval(animation);
				break;
			case 'btn-play':
				play = true;
				$('#btn-play').hide()
				$('#btn-pause').show()
				draw()
				break;
			case 'btn-backward':
				play = false;
				$('#btn-pause').hide()
				$('#btn-play').show()
				clearInterval(animation);
				animationIndex--
				draw()
				break;
			case 'btn-forward':
				play = false;
				$('#btn-pause').hide()
				$('#btn-play').show()
				clearInterval(animation);
				animationIndex++
				draw()
				break;
		}
	})
	$('.btn-reference').on('click', function(e) {
		let id = this.id;
		if (id === 'btn-show-reference') {
			reference = true
			$('#btn-hide-reference').show()
			$('#btn-show-reference').hide()
			draw()
		} else if (id === 'btn-hide-reference') {
			reference = false
			$('#btn-show-reference').show()
			$('#btn-hide-reference').hide()
			draw()
		}
	})
	var draw = function() {
		$.getJSON('./ccni_example.json', function(data) {

			datum = data[0]
			// var dotTrack = d3.histoDotTrack().width(window.innerWidth - 25).data(datum);

			if (split) {
				$('#barTracks svg').remove()
				for (var i = 0; i < data.length - 1; i++) {

					switch (graph) {
						case "bar":
							$('#barTracks').append(`
						<div class="barTrack track" id="barTrack_${i}"></div>
					`)
							var barTrack = d3.histoBarTrack().width(window.innerWidth - 25).data(data[i])
							d3.select(`#barTrack_${i}`).call(barTrack);
							break;
						case "dot":
							$('#barTracks').append(`
						<div class="dotTrack track" id="dotTrack_${i}"></div>
					`)
							var dotTrack = d3.histoDotTrack().width(window.innerWidth - 25).data(data[i])
							d3.select(`#dotTrack_${i}`).call(dotTrack);
							break;
					}

					if (reference) {
						var blockTrack = d3.blockTrack().width(window.innerWidth - 25).data(data[data.length - 1]);
						switch (graph) {
							case "bar":
								d3.select(`#barTrack_${i}`).call(blockTrack);
								break;
							case "dot":
								d3.select(`#dotTrack_${i}`).call(blockTrack);
								break;
						}

					}
				}
			} else if (play) {
				var track = null
				switch (graph) {
					case "bar":
						$('#barTracks').append(`<div class="barTrack track" id="barTrack"></div>`)
						$('#barTracks svg').hide(1000)
						$('#barTracks svg').remove()
						$('#dotTracks svg').hide(1000)
						$('#dotTracks svg').remove()


						track = d3.histoBarTrack().width(window.innerWidth - 25).data(data[0]);
						d3.select('#barTrack').call(track);
						if (reference) {
							var blockTrack = d3.blockTrack().width(window.innerWidth - 25).data(data[data.length - 1]);
							d3.select('#barTrack').call(blockTrack);
						}
						// d3.select('#dotTrack').call(dotTrack);
						break;
					case "dot":
						$('#barTracks').append(`<div class="dotTrack track" id="dotTrack"></div>`)
						$('#barTracks svg').hide(1000)
						$('#barTracks svg').remove()
						$('#dotTracks svg').hide(1000)
						$('#dotTracks svg').remove()

						track = d3.histoDotTrack().width(window.innerWidth - 25).data(data[0]);
						d3.select('#dotTrack').call(track);
						if (reference) {
							var blockTrack = d3.blockTrack().width(window.innerWidth - 25).data(data[data.length - 1]);
							d3.select('#dotTrack').call(blockTrack);
						}
				// d3.select('#dotTrack').call(dotTrack);
				break;
			}
			let cycle = function() {

				track.data(data[animationIndex])
				animationIndex++;
				if (animationIndex >= data.length - 1) {
					animationIndex = 0;
				}
			}
			animation = setInterval(cycle, 750)

		} else {
			if (animationIndex >= data.length - 1) {
				animationIndex = 0;
			} else if (animationIndex < 0) {
				animationIndex = data.length - 2;
			}
			var track = null
			switch (graph) {
				case "bar":
					$('#barTracks').append(`<div class="barTrack track" id="barTrack"></div>`)
					$('#barTracks svg').hide(1000)
					$('#barTracks svg').remove()
					$('#dotTracks svg').hide(1000)
					$('#dotTracks svg').remove()


					track = d3.histoBarTrack().width(window.innerWidth - 25).data(data[animationIndex]);
					d3.select('#barTrack').call(track);
					if (reference) {
						var blockTrack = d3.blockTrack().width(window.innerWidth - 25).data(data[data.length - 1]);
						d3.select('#barTrack').call(blockTrack);
					}
					// d3.select('#dotTrack').call(dotTrack);
					break;
				case "dot":
					$('#barTracks').append(`<div class="dotTrack track" id="dotTrack"></div>`)
					$('#barTracks svg').hide(1000)
					$('#barTracks svg').remove()
					$('#dotTracks svg').hide(1000)
					$('#dotTracks svg').remove()

					track = d3.histoDotTrack().width(window.innerWidth - 25).data(data[animationIndex]);
					d3.select('#dotTrack').call(track);
					if (reference) {
						var blockTrack = d3.blockTrack().width(window.innerWidth - 25).data(data[data.length - 1]);
						d3.select('#dotTrack').call(blockTrack);
					}
			// d3.select('#dotTrack').call(dotTrack);
			break;
		}


		}

	})
}
draw();

$('.tracks').on('mouseenter', '.track', function() {
	$(this).addClass('hover');
}).on('mouseleave', '.track', function() {
	$(this).removeClass('hover');
});
});
